"use strict";

import Node from '../../src/node';

describe('Node', () => {
    beforeEach(function() {
        // Create a simple document with
        // a div.content and a filled in <select>
        let select = document.createElement("select");
        for (let i = 0; i < 4; i++) {
            let option = document.createElement("option");
            option.textContent = "option " + i;
            select.appendChild(option);
        }

        let content = document.createElement("div");
        content.classList.add("content");
        document.body.appendChild(content);
        document.body.appendChild(select);

        this.select = select;
    });

    describe('mousedownEvent', () => {
        it('should instantiate a new Selector on the first invocation', function() {
        });
    });
});
