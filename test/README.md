# JavaScript Testing

This directory contains the JavaScript test suite.
This tests the client facing code that a person generally interacts with to provide dynamic content on the site.

The tests themselves are written using a testing framework called [Jasmine](https://jasmine.github.io/).
There are examples on their website, but the general idea is behavioral testing.
You have a file the corresponds to each JavaScript class or module and then within that a describe block for each function or instance method.
Within the describe block, there will be a set of tests that go through each expected behavior of that function.

The [karma](https://karma-runner.github.io/) tool is then used to start a set of browsers to execute the jasmine test suite.
Currently, at least the Chrome/Chromium browsers are targetted and will run in a headless mode.
Karma allows us to test on a variety of browsers and perform continuous integration, which means we are testing as we develop as much as possible automatically.

The Karma tool will skip any browser targets you don't have installed.
The more the better, of course, for local testing, but its really beneficially for continuous integration where we will deploy our tests on an infrastructre that guarantees the browsers we care about.
With services such as Travis, we can then test on a variety of browsers without maintaining a local archive of them ourselves. (Although we do care about doing that, as a project!)

Karma will look for any files in the `./tests` directory that end in `_spec.js`, such as `./tests/unit/node_spec.js`.
If you add such a file, it will automatically be executed as part of the test suite.
See `Writing Tests` further down this page for more information about conventions and writing tests.

## Running the Tests

To run the tests, you can use `npm test` or run them directly using `npx`:

```
npx karma start
```

The last line should show the number of tests passed (and if any failed) and you want to see a green SUCCESS message.

## Coverage Reports

We are using [Istanbul](https://istanbul.js.org/) to generate our HTML coverage reports for JavaScript tests.
When the test suite runs, by default, it will generate a set of HTML in the `./coverage` directory.

This will give you a general file listing and coverage status per file.
When you navigate to the file, you will see a code listing showing each line that was hit during testing (and how many times) and
each line that was not (marked in red).

## Writing Tests

A Jasmine cheatsheet that I find useful is [this one](https://devhints.io/jasmine).
