"use strict";

import Port from "./port.js";
import Util from "./util.js";
import JobDonut from "./job_donut.js";

import Node from "./node.js";

// As a design goal, most components will have a static create function that
// will generate the DOM for the component, and then a more specific fromJSON
// function which fills in the DOM according to the data given.
//
// The constructor, then, pulls out the data from the DOM. That way, the DOM
// can be prepopulated and the content can work without JavaScript (styled,
// visual depiction of a graph) even if you cannot move things around or
// interact dynamically.

/**
 * This represents a node in the workflow graph.
 */
class Group extends Node {
    /**
     * Creates a Node based on the given DOM element.
     *
     * @param {HTMLElement} element The `<li>` element representing the node.
     */
    constructor(element, options) {
        super(element, options);
        this._group = [];
    }

    /**
     * Returns the index of this Node.
     *
     * @returns {Number} The index. The first node is at index 0.
     */
    get index() {
        return super.index;
    }

    /**
     * Nodes can be selected.
     */
    get selectable() {
        return super.selectable;
    }

    /**
     * Whether or not the component is currently movable.
     */
    get movable() {
        return super.movable;
    }

    /**
     * Nodes can be selected as part of regions.
     */
    get regionSelectable() {
        return super.regionSelectable;
    }

    /**
     * Nodes can be collided.
     */
    get collidable() {
        return super.collidable;
    }

    /**
     * Get the node target information.
     */
    get targets() {
        return super.targets;
    }

    /**
     * Sets the node target information or removes it if null.
     */
    set targets(info) {
        super.targets = info;
    }

    /**
     * Get the node's icon.
     */
    get icon() {
        return super.icon;
    }

    /**
     * Update the node's icon.
     */
    set icon(value) {
        super.icon = value;
    }

    /**
     * Returns the visibility of this Node.
     */
    get visibility() {
        return super.visibility;
    }

    /**
     * Sets the visibility of this Node.
     *
     * @params {string} value The visibility value, based on CSS: "visible" or "hidden".
     */
    set visibility(value) {
        super.visibility = value;
    }

    /**
     * Returns all of the user-defined metadata for this node.
     */
    get data() {
        return super.data;
    }

    /**
     * Resets the given node metadata data to the given object.
     */
    set data(value) {
        super.data = value;
    }

    /**
     * Retrieve a particular metadata value for this node.
     */
    getData(key) {
        return super.getData(key);
    }

    /**
     * Set a particular metadata value for this node.
     */
    setData(key, value) {
        super.setData(key, value);
    }

    /**
     * Destroy this node which means disconnecting everything.
     */
    destroy() {
        super.destroy();
    }

    /**
     * Retrieves a comprehensive list of all Ports, including inputs, outputs,
     * and general ports.
     *
     * @returns {Array} The list of Port objects.
     */
    get allPorts() {
        return super.allPorts;
    }

    /**
     * Based on the current status, select or unselect this Node.
     */
    toggle() {
        super.toggle();
    }

    /**
     * Whether or not this Node is currently selected.
     */
    get selected() {
        return super.selected;
    }

    /**
     * Selects this Node.
     */
    select() {
        super.select();
    }

    /**
     * Unselects this Node.
     */
    unselect() {
        super.unselect();
    }

    /**
     * Marks this Node as being 'viewed'.
     */
    view() {
        super.view();
    }

    /**
     * Clears this Node as being 'viewed'.
     */
    unview() {
        super.unview();
    }

    /**
     * Marks this Node as being 'highlighted'.
     */
    highlight() {
        super.highlight();
    }

    /**
     * Clears this Node as being 'highlighted'.
     */
    unhighlight() {
        super.unhighlight();
    }

    /**
     * Reorganizes the ports on the DOM.
     */
    rearrangePorts() {
        super.rearrangePorts();
    }

    /**
     * Moves the node.
     *
     * @param {number} x The x world coordinate to position the node.
     * @param {number} y The y world coordinate to position the node.
     */
    move(x, y) {
        super.move(x, y);
    }

    /**
     * Redraws the node and the attached wires.
     */
    redraw() {
        super.redraw();
    }

    /**
     * Retrieve the general Port at the given index.
     *
     * @returns {Port} The Port at that given index or undefined.
     */
    portAt(index) {
        return super.portAt(index);
    }

    /**
     * Retrieve the input Port at the given index.
     *
     * @returns {Port} The Port at that given index or undefined.
     */
    inputAt(index) {
        return super.inputAt(index);
    }

    /**
     * Retrieve the output Port at the given index.
     *
     * @returns {Port} The Port at that given index or undefined.
     */
    outputAt(index) {
        return super.outputAt(index);
    }

    /**
     * Retrieves the list of general ports.
     */
    get ports() {
        return super.ports;
    }

    /**
     * Retrieves the list of input ports.
     */
    get inputs() {
        return super.inputs;
    }

    /**
     * Retrieves the list of output ports.
     */
    get outputs() {
        return super.outputs;
    }

    /**
     * Retrieves the list of general ports that are currently hidden.
     */
    get hiddenPorts() {
        return super.hiddenPorts;
    }

    /**
     * Retrieves the list of input ports that are currently hidden.
     */
    get hiddenInputs() {
        return super.hiddenInputs;
    }

    /**
     * Retrieves the list of output ports that are currently hidden.
     */
    get hiddenOutputs() {
        return super.hiddenOutputs;
    }

    /**
     * Reveals the dropdown for any hidden general ports, of any.
     */
    showPortDropdown(button) {
        super.showPortDropdown(button);
    }

    /**
     * Reveals the dropdown for any hidden input ports, of any.
     */
    showInputDropdown(button) {
        super.showInputDropdown(button);
    }

    /**
     * Reveals the dropdown for any hidden output ports, of any.
     */
    showOutputDropdown(button) {
        super.showOutputDropdown(button);
    }

    /**
     * Hides any dropdown that is currently displayed.
     */
    hideDropdown() {
        super.hideDropdown();
    }

    /**
     * Updates a job related to this node.
     *
     * Will add the job if the given id is unique.
     *
     * This will render the job status graphically.
     */
    updateJob(info) {
        super.updateJob(info);
    }

    /**
     * Updates the node to reflect the given serialization.
     *
     * @param {object} json The pre-parsed JSON serialization.
     */
    fromJSON(json) {
        super.fromJSON(json);

        // json.group.forEach((node) => {
        //     this._addNode(node);

        // });
    }

    /**
     * Adds a node to this group.
     */
    _addNode(nodeJSON) {
        // let element = this.element;
        const nodeElement = document.createElement("li");
        // Grab the group list
        const group = this.element.querySelector("ul.group");
        const node = Node.create(nodeJSON, this._options, nodeElement);
        group.appendChild(nodeElement);
        this._group.push(node);
        return node;
    }


    /**
     * Adds a port to this node.
     */
    createPort(portType, portInfo) {
        super.createPort(portType, portInfo);
    }

    /**
     * Ensures that the "reveal port" buttons are visible when they are valid.
     */
    updatePortAddButtons() {
        super.updatePortAddButtons();
    }

    /**
     * Returns a JSON-ready serialization of the node.
     *
     * @returns {object} The JSON-ready serialization of the node.
     */
    toJSON() {
        return super.toJSON();
    }

    static _create_element(options, element = null) {
        element = super._create_element(options, element);

        let groupNodes = element.querySelector("ul.group");
        if (!groupNodes) {
            groupNodes = document.createElement("ul");
        }
        groupNodes.classList.add("group");
        element.appendChild(groupNodes);

        return element;
    }

    /**
     * Creates a DOM element for a Node from the given node JSON serialization.
     *
     * @param {object} json The pre-parsed JSON serialization of the node.
     * @param {object} options The general workflow options.
     *
     * @returns {Node} The instantiated Node object.
     */
    static create(json, options, element = null) {
        element = Group._create_element(options, element);

        let group = new Group(element, options);
        if (json) {
            group.fromJSON(json);
        }

        return group;
    }
}

export default Group;
