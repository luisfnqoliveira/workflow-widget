"use strict";

import Workflow from "../../src/workflow.js";

var graph = {
    "center": {
        "x": 50,
        "y": 200
    },
    "connections": [
        {
            "ports": [
                {
                    "name": "moo",
                    "type": "bar"
                }
            ],
            "type": "Component",
            "name": "Miranda",
            "data": {
                "component-name": "cpu",
                "component-type": "miranda.BaseCPU"
            }
        },
        {
            "name": "Cache",
            "type": "Component",
            "position": {"y": 300},
            "data": {
                "component-name": "l1cache",
                "component-type": "memHierarchy.Cache"
            }
        },
        {
            "ports": [
                {
                    "name": "foo",
                    "type": "bar"
                }
            ],
            "name": "MemController",
            "type": "Component",
            "icon": "images/simulator.svg",
            "position": {"y": 150},
            "data": {
                "component-name": "memory",
                "component-type": "memHierarchy.MemController"
            }
        }
    ]
};

(function bootstrap() {
    let workflow_element = document.querySelectorAll("workflow-widget")[0];
    let workflow = new Workflow(workflow_element, {
        buttons: [
            {
                classes: ["view-button"]
            },
            {
                classes: ["configure-button"]
            }
        ]
    }, graph);

    workflow.on("button-click", (event) => {
        console.log(event);
    });
})();
