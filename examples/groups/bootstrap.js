"use strict";

import Workflow from "../../src/workflow.js";

var graph = {
    "center": {
        "x": 50,
        "y": 200
    },
    "connections": [
        {
            "name": "Group1",
            "type": "Group",
            "contains": {
                "center": {
                    "x": 50,
                    "y": 200
                },
                "connections": [
                    {
                        "type": "Component",
                        "name": "BaseCPU",
                        "icon": "./images/simulator.svg",
                        "position": {
                            "x": 300,
                            "y": 300
                        },
                        "target": {},
                        "ports": [
                            {
                                "type": "SST::Miranda::RequestGenerator",
                                "name": "generator",
                                "connections": []
                            },
                            {
                                "type": "SST::Interfaces::StandardMem",
                                "name": "memory",
                                "visibility": "hidden",
                                "connections": []
                            },
                            {
                                "type": "",
                                "name": "cache_link",
                                "connections": [
                                    {
                                        "to": {
                                            "node": 1,
                                            "port": 8,
                                            "wire": 0
                                        }
                                    }
                                ]
                            },
                            {
                                "type": "",
                                "name": "src",
                                "visibility": "hidden",
                                "connections": []
                            }
                        ],
                        "data": {
                            "sst-component-path": "miranda.BaseCPU",
                            "sst-component-name": "BaseCPU",
                            "sst-is-subcomponent": false,
                            "object-uid": "QmX5pxeD96LyspHq3mLfKffUd2wmKAQHXsT9EDZUanTUh2",
                            "object-name": "SST Elements",
                            "object-type": "component",
                            "object-subtype": [
                                "SST"
                            ]
                        }
                    },
                    {
                        "type": "Component",
                        "name": "Cache",
                        "icon": "./images/simulator.svg",
                        "position": {
                            "x": 800,
                            "y": 300
                        },
                        "target": {},
                        "ports": [
                            {
                                "type": "SST::MemHierarchy::MemLinkBase",
                                "name": "cpulink",
                                "visibility": "hidden",
                                "connections": []
                            },
                            {
                                "type": "SST::MemHierarchy::MemLinkBase",
                                "name": "memlink",
                                "visibility": "hidden",
                                "connections": []
                            },
                            {
                                "type": "SST::MemHierarchy::CoherenceController",
                                "name": "coherence",
                                "visibility": "hidden",
                                "connections": []
                            },
                            {
                                "type": "SST::MemHierarchy::CacheListener",
                                "name": "prefetcher",
                                "visibility": "hidden",
                                "connections": []
                            },
                            {
                                "type": "SST::MemHierarchy::CacheListener",
                                "name": "listener",
                                "visibility": "hidden",
                                "connections": []
                            },
                            {
                                "type": "SST::MemHierarchy::ReplacementPolicy",
                                "name": "replacement",
                                "visibility": "hidden",
                                "connections": []
                            },
                            {
                                "type": "SST::MemHierarchy::HashFunction",
                                "name": "hash",
                                "visibility": "hidden",
                                "connections": []
                            },
                            {
                                "type": "",
                                "name": "low_network_0",
                                "connections": []
                            },
                            {
                                "type": "",
                                "name": "high_network_0",
                                "connections": [
                                    {
                                        "to": {
                                            "node": 0,
                                            "port": 2,
                                            "wire": 0
                                        }
                                    }
                                ]
                            },
                            {
                                "type": "",
                                "name": "directory",
                                "visibility": "hidden",
                                "connections": []
                            },
                            {
                                "type": "",
                                "name": "directory_ack",
                                "visibility": "hidden",
                                "connections": []
                            },
                            {
                                "type": "",
                                "name": "directory_fwd",
                                "visibility": "hidden",
                                "connections": []
                            },
                            {
                                "type": "",
                                "name": "directory_data",
                                "visibility": "hidden",
                                "connections": []
                            },
                            {
                                "type": "",
                                "name": "cache",
                                "visibility": "hidden",
                                "connections": []
                            },
                            {
                                "type": "",
                                "name": "cache_ack",
                                "visibility": "hidden",
                                "connections": []
                            },
                            {
                                "type": "",
                                "name": "cache_fwd",
                                "visibility": "hidden",
                                "connections": []
                            },
                            {
                                "type": "",
                                "name": "cache_data",
                                "visibility": "hidden",
                                "connections": []
                            }
                        ],
                        "data": {
                            "sst-component-path": "memHierarchy.Cache",
                            "sst-component-name": "Cache",
                            "sst-is-subcomponent": false,
                            "object-uid": "QmX5pxeD96LyspHq3mLfKffUd2wmKAQHXsT9EDZUanTUh2",
                            "object-name": "SST Elements",
                            "object-type": "component",
                            "object-subtype": [
                                "SST"
                            ]
                        }
                    }
                ]
            }
        },
        {
            "name": "Cache",
            "type": "Component",
            "position": {"y": 300},
            "data": {
                "component-name": "l1cache",
                "component-type": "memHierarchy.Cache"
            }
        },
        {
            "ports": [
                {
                    "name": "foo",
                    "type": "bar"
                },
                {
                    "name": "foo2",
                    "type": "bar2"
                },{
                    "name": "foo3",
                    "type": "bar3"
                }
            ],
            "name": "MemController",
            "type": "Component",
            "icon": "images/simulator.svg",
            "position": {"y": 150},
            "data": {
                "component-name": "memory",
                "component-type": "memHierarchy.MemController"
            },
            "inputs": [
                {
                    "name": "foo",
                    "type": "bar"
                },{
                    "name": "foo2",
                    "type": "bar2"
                },{
                    "name": "foo3",
                    "type": "bar3"
                }
            ]
        }
    ]
};

(function bootstrap() {
    let workflow_element = document.querySelectorAll("workflow-widget")[0];
    let workflow = new Workflow(workflow_element, {
        buttons: [
            {
                classes: ["view-button"]
            },
            {
                classes: ["configure-button"]
            }
        ]
    }, graph);

    workflow.on("button-click", (event) => {
        console.log(event);
    });
})();
