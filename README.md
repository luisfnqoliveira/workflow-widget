# Workflow UI Widget

## Overview

This is a JavaScript widget that enables the visual editing of workflows, which are graphs that depict an optionally ordered set of tasks.

## Usage

You can use the packaged JavaScript in the [dist](dist) directory like so:

```
<script src="workflow.js"></script>
```

Or modular:

```
<script type="module" src="workflow.js">
```

Or by import:

```
import Workflow from "workflow"
```

And then you can simply add an "workflow-widget" element to your page. Or use it programmably:

```javascript
let myElement = document.querySelector("#my-workflow-widget");
let workflow = new Workflow(myElement, {
  /* options here, see below */
});
```

### Serialization

To get a serialization, just call the `toJSON` method which gives a JS object representing the workflow. You can then simply `JSON.stringify` the result if you need a string serialization. Conversely, you can use `fromJSON` to take such a serialization and build a workflow based on it.

```
// Export to JSON
let serialized = workflow.toJSON();
let json = JSON.stringify(serialized);

/* send or store json here */

// Parse JSON
let incoming = JSON.parse(json);
workflow.fromJSON(incoming);
```

### Examples

You can look at a simple example in [examples/simple](examples/simple).

## Options

### horizontalSnapTolerance

**Default**: 10. The size, in pixels, of the snap tolerance in the horizontal (left-to-right) direction. That is, when you are dragging a node, when the node gets close to an existing node's X position, their own X position will *snap* to share that X position. This allows an easier method of lining up nodes. If you set this to 0, this turns off this feature.

### verticalSnapTolerance

**Default**: 20. The size, in pixels, of the snap tolerance in the vertical (top-to-bottom) direction. That is, when you are dragging a node, when the node gets close to an existing node's Y position, their own Y position will *snap* to share that Y position. This allows an easier method of lining up nodes. If you set this to 0, this turns off this feature.

### allowPanning

**Default**: true. When `true`, this allows the movement of the workflow visualization as a whole. Otherwise, the workflow should be programmably moved in order to show any relevant part of the workflow.

### allowSelections

**Default**: true. When `true`, this allows a subset nodes to be selected by dragging on the surface or through the shift + click shortcut.

### imageBaseURL

**Default**: "". When set, this field will be prepended directly to any Node's `icon` value. This field is not particularly smart about slashes. That is, the concatenation is naïve. Normally, you would want a final slash such that if `imageBaseURL` is "images/icons/", then a Node with an `icon` field of "blah.png" would resolve to "images/icons/blah.png".

## Workflow JSON Specification

```
{
  "center": { "x": 0, "y": 0 },
  "connections": [
    /* description of each Node */
  ]
}
```

The **center** key gives the world coordinate (x, y) pair for the world coordinate that should be at the center of the visualization. This ensures that, regardless to the size of the viewport, the same center of focus is retained when deserializing this workflow. Each `x` and `y` coordinate is a Number.

The **connections** key is a list of descriptions for each Node. Such a description follows:

### Node

```
{
  "icon": "images/simulator.svg",
  "name": "My Simulator",
  "type": "simulator",
  "visibility": "visible",
  "position": { "x": 0, "y": 0 },
  "inputs": [
    /* description of each Port */
  ],
  "outputs": [
    /* description of each Port */
  ],
  "ports": [
    /* description of each Port */
  ]
}
```

The **position** key gives the world coordinate (x, y) pair for the world coordinate of that Node's top-left corner. Each `x` and `y` is a Number.

The **icon** key is a string that identifies the relative or absolute path of the image for this Node. You can specify the `imageBaseURL` option as an absolute or relative path that will be prepended to this string. This such a field set, the `icon` field would always be relative to the `imageBaseURL` and can no longer be absolute.

The **name** field is a string that represents the human name for this Node.

The **type** field is an optional field that can add a representative human type to the Node. A type may represent a category or component type for the Node that may be semantically interesting.

The **visibility** field dictates whether this Node (and its ports and connections) are visible. The field is either "visible" or "hidden", which corresponds to the CSS definition of the `visibility` key. Defaults to "visible".

The **inputs**, **outputs**, and **ports** fields are all lists of descriptions of any Port that serves, respectively, as an input, output, or general/bi-directional port relative to this Node. Input ports can only be connected to output ports, and vice versa. General ports can only be connected to other general ports (and cannot connect to those ports designated as an "input" or "output".)

Each Port within any of the "inputs", "outputs" and "ports" fields has the following description:

### Port

```
{
  "name": "Trace Input",
  "type": "Trace",
  "visibility": "visible",
  "max": 1,
  "connections": [
    /* description of each Wire */
  ]
}
```

The **name** field, here, specifies the name of the Port.

The **type** field specifies the expected type of data that should be connected here.

The **max** field provides a maximum number of connections allowed. When this max is hit, no further wires can be added.

The **visibility** field dictates whether this Port (and its connections) are visible. The field is either "visible" or "hidden", which corresponds to the CSS definition of the `visibility` key. Defaults to "visible".

The **connections** field holds a list of, well, connections between two Ports within this workflow. Each such Wire has the following description:

### Wire

```
{
  "to": {
    "node": 0,
    "port": 1,
    "wire": 0
  }
}
```

Simply, the **to** field holds indices for the **node**, which corresponds to the Node order in the workflow's main `connections` field, the **port** which is the corresponding Port in the corresponding `connections` field of the opposing Port at that node, and **wire** which is the index of the connection at that Port.

If this is a Wire described within an "input" Port, then the `port` here describes the index of the Port at the indicated Node's `outputs`, and vice versa for a Wire within an "output" Port. For a general Port, that is to say a Wire within a `ports` field, it looks at the indicated Node's own `ports` field.

When a Node is Wire is deleted when constructing or modifying a workflow, these indices may change rather wildly. You should not expect the indices to be easily referential outside of the context of the workflow. That is, you should not often refer (or do so very carefully) to a Wire or Port or even Node by index in external metadata.

## Development

You can install the project dependencies via npm:

```
npm install
```

You can build a distribution, where all sources are packed together via webpack:

```
npx webpack
```

You can build the documentation, which puts its output rooted at `docs/index.html`:

```
npm run build-docs
```

### Testing

For testing the client facing JavaScript code, we will be using [karma](https://karma-runner.github.io/) alongside
[Jasmine](https://jasmine.github.io/). The tests are located in `./tests` and are invoked with:

```
npm test
```

or directly:

```
npx karma start
```

This will run the test suite using any headless browser that you have currently installed that has also been configured
in our testing configuration in `karma.conf.js`.

For more information, see [the tests directory](tests/README.md).

## Contributing

